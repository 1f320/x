# Miscellaneous Go packages

[![Go Reference](https://pkg.go.dev/badge/1f320.xyz/x.svg)](https://pkg.go.dev/1f320.xyz/x)
[![godocs.io](https://godocs.io/1f320.xyz/x?status.svg)](https://godocs.io/1f320.xyz/x)

Miscellaneous Go packages that are too small to warrant their own repositories but too big to copy-paste every time

## `colours`

Provides colour constants and a function to get the average colour from an image.

## `parameters`

Provides `parameters.Parameters` and `parameters.Flags` types, both to make it easier to work with command-line style input in non-terminal contexts.

`parameters.Parameters` is pretty much a direct port of [PluralKit](https://github.com/xSke/PluralKit)'s command parser to Go.

## `duration`

Provides functions to humanize durations.

## `strings`

Provides a couple string-related functions.

## `concurrent`

Provides thread-safe, generic Value, Map, Set, and OrderedSet types.

## `optional`

Provides a generic Optional type to consume JSON.
