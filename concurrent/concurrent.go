// Package concurrent provides a handful of thread-safe generic types.
package concurrent

import "sync"

// entry is used to make the implementations of Set slightly nicer.
type entry struct{}

// Value wraps a single value. A zero Value is valid.
// The wrapped type should not be a pointer.
type Value[T any] struct {
	val T
	mu  sync.RWMutex
}

func (s *Value[T]) Set(val T) {
	s.mu.Lock()
	s.val = val
	s.mu.Unlock()
}

func (s *Value[T]) Get() T {
	s.mu.Lock()
	v := s.val
	s.mu.Unlock()
	return v
}
