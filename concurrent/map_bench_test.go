package concurrent_test

import (
	"sync"
	"testing"

	"gitlab.com/1f320/x/concurrent"
)

func BenchmarkStdlibMap(b *testing.B) {
	m := make(map[int]string)
	var mu sync.RWMutex

	for i := 0; i < b.N; i++ {
		mu.Lock()
		m[i] = "a"
		mu.Unlock()
	}
}

func BenchmarkConcurrentMap(b *testing.B) {
	m := concurrent.NewMap[int, string]()

	for i := 0; i < b.N; i++ {
		m.Set(i, "a")
	}
}
