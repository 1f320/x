package concurrent_test

import (
	"testing"

	"gitlab.com/1f320/x/concurrent"
	"github.com/matryer/is"
)

func TestMapFunc(t *testing.T) {
	is := is.New(t)

	m := concurrent.NewMap[int, string]()

	m.WriteFunc(func(m map[int]string) {
		for i := 0; i < 20; i++ {
			m[i] = "a"
		}
	})

	m.ReadFunc(func(m map[int]string) {
		for i := 0; i < 20; i++ {
			_, ok := m[i]
			is.True(ok)
		}
	})

	for i := 0; i < 20; i++ {
		is.True(m.Exists(i))
	}
}
