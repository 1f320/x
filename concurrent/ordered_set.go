package concurrent

import "sync"

// OrderedSet has the same usage as Set, but is ordered.
type OrderedSet[T comparable] struct {
	m     map[T]entry
	order []T
	mu    sync.RWMutex
}

// NewOrderedSet returns a new OrderedSet.
func NewOrderedSet[T comparable](initial ...T) *OrderedSet[T] {
	set := &OrderedSet[T]{
		m:     make(map[T]entry, len(initial)),
		order: make([]T, 0, len(initial)),
	}

	set.order = append(set.order, initial...)
	for i := range initial {
		set.m[initial[i]] = entry{}
	}

	return set
}

// Add adds a value to the set. It returns true if the value doesn't already exist, false otherwise.
func (s *OrderedSet[T]) Add(v T) bool {
	s.mu.Lock()
	_, exists := s.m[v]
	s.m[v] = entry{}
	s.mu.Unlock()

	if !exists {
		s.order = append(s.order, v)
	}

	return !exists
}

// Append adds a slice of values to the set.
func (s *OrderedSet[T]) Append(values ...T) {
	s.mu.Lock()
	for _, v := range values {
		_, exists := s.m[v]
		if !exists {
			s.m[v] = entry{}
			s.order = append(s.order, v)
		}
	}
	s.mu.Unlock()
}

// Remove removes a value from the set. It returns true if the value existed in the set, false otherwise.
func (s *OrderedSet[T]) Remove(v T) (exists bool) {
	s.mu.Lock()
	_, exists = s.m[v]

	delete(s.m, v)
	s.order = remove(s.order, v)
	s.mu.Unlock()

	return exists
}

// Exists returns true if v exists in the set, false otherwise.
func (s *OrderedSet[T]) Exists(v T) (exists bool) {
	s.mu.RLock()
	_, exists = s.m[v]
	s.mu.RUnlock()
	return exists
}

// Values returns all values in the set. The return slice is unordered.
func (s *OrderedSet[T]) Values() []T {
	s.mu.RLock()
	values := make([]T, 0, len(s.m))
	values = append(values, s.order...)
	s.mu.RUnlock()
	return values
}

// Length returns the length of the set.
func (s *OrderedSet[T]) Length() int {
	s.mu.RLock()
	length := len(s.m)
	s.mu.RUnlock()
	return length
}

// remove removes the given value in slice.
func remove[T comparable](slice []T, val T) []T {
	for i := range slice {
		if slice[i] == val {
			if i == 0 {
				slice = slice[1:]
			} else if i == len(slice)-1 {
				slice = slice[:len(slice)-1]
			} else {
				slice = append(slice[:i], slice[i+1:]...)
			}
			break
		}
	}
	return slice
}
