package concurrent_test

import (
	"testing"

	"gitlab.com/1f320/x/concurrent"
)

func BenchmarkSet(b *testing.B) {
	set := concurrent.NewSet[int]()

	for i := 0; i < b.N; i++ {
		set.Add(i)
	}

	for i := 0; i < b.N; i++ {
		set.Remove(i)
	}
}

func BenchmarkOrderedSet(b *testing.B) {
	set := concurrent.NewOrderedSet[int]()

	for i := 0; i < b.N; i++ {
		set.Add(i)
	}

	for i := 0; i < b.N; i++ {
		set.Remove(i)
	}
}
