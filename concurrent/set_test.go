package concurrent_test

import (
	"testing"

	"gitlab.com/1f320/x/concurrent"
	"github.com/matryer/is"
)

func TestSet(t *testing.T) {
	is := is.New(t)

	set := concurrent.NewSet(
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
	)

	is.True(set.Remove(1))

	is.Equal(set.Exists(1), false)
	is.Equal(set.Exists(4), true)
	is.Equal(set.Exists(11), false)

	is.Equal(set.Length(), 9)
}

func TestOrderedSet(t *testing.T) {
	is := is.New(t)

	set := concurrent.NewOrderedSet(
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
	)

	is.True(set.Remove(1))
	is.True(set.Remove(7))

	is.True(!set.Remove(58))

	is.Equal(set.Exists(1), false)
	is.Equal(set.Exists(4), true)
	is.Equal(set.Exists(11), false)

	is.Equal(set.Length(), 8)
	is.Equal(
		set.Values(),
		[]int{2, 3, 4, 5, 6, 8, 9, 10},
	)
}
