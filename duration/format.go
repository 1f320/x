// Package duration implements functions to turn durations into a human-readable format.
package duration

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"
)

const (
	day = 24 * time.Hour

	year  = 365 * day
	month = 30 * day
)

// years returns the number of years in d, and the total time not in a year.
func years(d time.Duration) (years int, left time.Duration) {
	years = int(math.Floor(float64(d.Seconds()/86400) / 365))

	return years, d - (time.Duration(years) * year)
}

// months returns the number of months in d, and the total time not in a month.
func months(d time.Duration) (months int, left time.Duration) {
	months = int(math.Floor(float64(d.Seconds()/86400) / 30))

	return months, d - (time.Duration(months) * month)
}

// days returns the number of days in d, and the total time not in a month.
func days(d time.Duration) (days int, left time.Duration) {
	days = int(math.Floor(float64(d.Seconds() / 86400)))

	return days, d - (time.Duration(days) * day)
}

// hours returns the number of hours in d, and the total time not in an hour.
func hours(d time.Duration) (hours int, left time.Duration) {
	hours = int(math.Floor(float64(d.Seconds() / 3600)))

	return hours, d - (time.Duration(hours) * time.Hour)
}

// minutes returns the number of minutes in d, and the total time not in an minute.
func minutes(d time.Duration) (minutes int, left time.Duration) {
	minutes = int(math.Floor(float64(d.Seconds() / 60)))

	return minutes, d - (time.Duration(minutes) * time.Minute)
}

// Format returns d as a human-readable string.
//
// If d is <1 month, the formats used are day, hour, minute, and second.
// If d is <1 year, the formats used are month, day, hour, and minute.
// In all other cases, the formats used are year, month, day, and hour.
//
// Months are always treated as 30 days, and years are always treated as 365 days.
func Format(d time.Duration) string {
	return FormatCustom(d, englishParts, joinEnglish)
}

// Parts are functions to transform a number into a human-readable string.
type Parts struct {
	Years   func(int) string
	Months  func(int) string
	Days    func(int) string
	Hours   func(int) string
	Minutes func(int) string
	Seconds func(int) string
}

var englishParts = Parts{
	Years:   pluralEnglish("year"),
	Months:  pluralEnglish("month"),
	Days:    pluralEnglish("day"),
	Hours:   pluralEnglish("hour"),
	Minutes: pluralEnglish("minute"),
	Seconds: pluralEnglish("second"),
}

// EnglishParts returns a copy of the default Parts for English.
func EnglishParts() Parts {
	p := englishParts
	return p
}

// Joiner is used to join the duration parts into the final string.
type Joiner func([]string) string

// FormatCustom is like Format, but allows you to specify your own Parts and Joiner, for localization.
//
// If d is <1 month, the formats used are day, hour, minute, and second.
// If d is <1 year, the formats used are month, day, hour, and minute.
// In all other cases, the formats used are year, month, day, and hour.
//
// Months are always treated as 30 days, and years are always treated as 365 days.
func FormatCustom(d time.Duration, p Parts, join Joiner) string {
	if months, _ := months(d); months < 1 {
		return formatWithoutMonth(d, p, join)
	} else if years, _ := years(d); years < 1 {
		return formatWithoutYear(d, p, join)
	}

	s := make([]string, 0, 4)

	years, d := years(d)
	if years > 0 {
		s = append(s, p.Years(years))
	}

	months, d := months(d)
	if months > 0 {
		s = append(s, p.Months(months))
	}

	days, d := days(d)
	if days > 0 {
		s = append(s, p.Days(days))
	}

	hours, d := hours(d)
	if hours > 0 {
		s = append(s, p.Hours(hours))
	}

	return join(s)
}

// formatWithoutYear formats d with no year unit, but with minutes.
func formatWithoutYear(d time.Duration, p Parts, join Joiner) string {
	s := make([]string, 0, 4)

	months, d := months(d)
	if months > 0 {
		s = append(s, p.Months(months))
	}

	days, d := days(d)
	if days > 0 {
		s = append(s, p.Days(days))
	}

	hours, d := hours(d)
	if hours > 0 {
		s = append(s, p.Hours(hours))
	}

	minutes, _ := minutes(d)
	if minutes > 0 {
		s = append(s, p.Minutes(minutes))
	}

	return join(s)
}

// formatWithoutMonth formats d with no month or year units, but with minute and second units.
func formatWithoutMonth(d time.Duration, p Parts, join Joiner) string {
	s := make([]string, 0, 4)

	days, d := days(d)
	if days > 0 {
		s = append(s, p.Days(days))
	}

	hours, d := hours(d)
	if hours > 0 {
		s = append(s, p.Hours(hours))
	}

	minutes, d := minutes(d)
	if minutes > 0 {
		s = append(s, p.Minutes(minutes))
	}

	if d.Seconds() > 0 {
		seconds := int(math.Round(d.Seconds()))
		s = append(s, p.Seconds(seconds))
	}

	return join(s)
}

func pluralEnglish(word string) func(int) string {
	return func(i int) string {
		if i == 1 {
			return "1 " + word
		}
		return strconv.Itoa(i) + " " + word + "s"
	}
}

func joinEnglish(s []string) string {
	switch len(s) {
	case 0:
		return ""
	case 1:
		return s[0]
	case 2:
		return strings.Join(s, " and ")
	default:
		return fmt.Sprintf("%s, and %s", strings.Join(s[:len(s)-1], ", "), s[len(s)-1])
	}
}
