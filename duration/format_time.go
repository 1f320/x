package duration

import (
	"fmt"
	"math"
	"time"
)

const (
	hourSeconds   = 60 * minuteSeconds
	minuteSeconds = 60
)

// FormatAt formats the duration between now and t.
func FormatAt(now, t time.Time) (s string, isBefore bool) {
	var years, months, days int

	now = now.In(t.Location())

	cur := now

	if t.Before(now) {
		isBefore = true
		t = now.Add(time.Since(t))
	}

	// loop for years
	for i := 0; i < 1000; i++ {
		newT := cur.AddDate(1, 0, 0)
		if newT.After(t) {
			break
		}
		// newT is more than a year after cur, so add 1 to years
		years++
		cur = newT
	}

	// loop for months
	for i := 0; i < 15; i++ {
		newT := cur.AddDate(0, 1, 0)
		if newT.After(t) {
			break
		}
		// newT is more than a month after cur, so add 1 to months
		months++
		cur = newT
	}

	// loop for days
	for i := 0; i < 32; i++ {
		newT := cur.AddDate(0, 0, 1)
		if newT.After(t) {
			break
		}
		// newT is more than a day after cur, so add 1 to days
		days++
		cur = newT
	}

	secs := float64(t.Sub(cur) / time.Second)

	hours := math.Floor(secs / hourSeconds)

	secs = math.Mod(secs, hourSeconds)

	minutes := math.Floor(secs / minuteSeconds)
	secs = math.Mod(secs, minuteSeconds)

	out := make([]string, 0, 6)

	if years != 0 {
		out = append(out, plural(years, "year"))
	}

	if months != 0 {
		out = append(out, plural(months, "month"))
	}

	if days != 0 {
		out = append(out, plural(days, "day"))
	}

	if hours != 0 {
		hours := int(hours)

		out = append(out, plural(hours, "hour"))
	}

	if minutes != 0 {
		minutes := int(minutes)

		out = append(out, plural(minutes, "minute"))
	}

	if secs != 0 {
		secs := int(secs)

		out = append(out, plural(secs, "second"))
	}

	return joinEnglish(out), isBefore
}

func plural(i int, word string) string {
	if i == 1 {
		return "1 " + word
	}
	return fmt.Sprintf("%d %ss", i, word)
}
