// Package optional implements an Optional[T] type for JSON use.
package optional

import "encoding/json"

// Optional is an optional value.
// When unmarshaling a struct, if the value is not present,
// o.Present and o.Null are false, and o.Value is the zero value for T.
//
// Note that Go doesn't let us control marshaling as much as unmarshaling,
// so the reverse is not possible.
// Optional is marshaled as null if o.Null is true or o.Present is false.
type Optional[T any] struct {
	Value   T
	Null    bool
	Present bool
}

var _ json.Unmarshaler = (*Optional[int])(nil)
var _ json.Marshaler = (*Optional[int])(nil)

// UnmarshalJSON implements json.Unmarshaler
func (o *Optional[T]) UnmarshalJSON(src []byte) error {
	if string(src) == "null" {
		o.Null = true
		o.Present = true

		return nil
	}

	o.Present = true
	return json.Unmarshal(src, &o.Value)
}

// MarshalJSON implements json.Marshaler
func (o Optional[T]) MarshalJSON() ([]byte, error) {
	if o.Null || !o.Present {
		return []byte("null"), nil
	}

	return json.Marshal(o.Value)
}

// Null returns an Optional[T] set to null.
func Null[T any]() Optional[T] {
	return Optional[T]{Null: true, Present: true}
}

// NewWithValue returns an Optional[T] with the value set to v.
func NewWithValue[T any](v T) Optional[T] {
	return Optional[T]{Value: v, Null: false, Present: true}
}
