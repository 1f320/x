package optional

import (
	"encoding/json"
	"testing"

	"github.com/matryer/is"
)

type testData struct {
	Name  string           `json:"name"`
	Value Optional[string] `json:"value"`
}

const setOptional = `{"name":"SetOptional","value":"Hello!"}`

func TestSetOptional(t *testing.T) {
	is := is.New(t)

	var td testData
	err := json.Unmarshal([]byte(setOptional), &td)
	is.NoErr(err)

	is.True(td.Value.Present)
	is.True(!td.Value.Null)
	is.Equal(td.Value.Value, "Hello!")
}

const unsetOptional = `{"name":"SetOptional"}`

func TestUnsetOptional(t *testing.T) {
	is := is.New(t)

	var td testData
	err := json.Unmarshal([]byte(unsetOptional), &td)
	is.NoErr(err)

	is.True(!td.Value.Present)
}

const nullOptional = `{"name":"SetOptional","value":null}`

func TestNullOptional(t *testing.T) {
	is := is.New(t)

	var td testData
	err := json.Unmarshal([]byte(nullOptional), &td)
	is.NoErr(err)

	is.True(td.Value.Present)
	is.True(td.Value.Null)
}
