package parameters

import (
	"flag"
	"strings"
)

type Flags struct {
	fs    *flag.FlagSet
	flags map[string]interface{}
}

func NewFlags(name string) *Flags {
	return &Flags{
		fs:    flag.NewFlagSet(name, flag.ContinueOnError),
		flags: map[string]interface{}{},
	}
}

// Bool sets a boolean flag.
func (f *Flags) Bool(name string, value bool, usage string) {
	p := f.fs.Bool(name, value, usage)
	f.flags[name] = p
}

// GetBool returns the flag called "name".
// ok is false if the flag is not found or if it isn't a boolean.
func (f *Flags) GetBool(name string) (val bool, ok bool) {
	v, ok := f.flags[name]
	if !ok {
		return false, false
	}
	ptr, ok := v.(*bool)
	if !ok {
		return false, false
	}
	return *ptr, true
}

// String sets a string flag.
func (f *Flags) String(name string, value string, usage string) {
	p := f.fs.String(name, value, usage)
	f.flags[name] = p
}

// GetString returns the flag called "name".
// ok is false if the flag is not found or if it isn't a string.
func (f *Flags) GetString(name string) (val string, ok bool) {
	v, ok := f.flags[name]
	if !ok {
		return "", false
	}
	ptr, ok := v.(*string)
	if !ok {
		return "", false
	}
	return *ptr, true
}

// Int sets an int64 flag.
func (f *Flags) Int(name string, value int64, usage string) {
	p := f.fs.Int64(name, value, usage)
	f.flags[name] = p
}

// GetInt64 returns the flag called "name".
// ok is false if the flag is not found or if it isn't an int.
func (f *Flags) GetInt64(name string) (val int64, ok bool) {
	v, ok := f.flags[name]
	if !ok {
		return 0, false
	}
	ptr, ok := v.(*int64)
	if !ok {
		return 0, false
	}
	return *ptr, true
}

// Float sets an float64 flag.
func (f *Flags) Float(name string, value float64, usage string) {
	p := f.fs.Float64(name, value, usage)
	f.flags[name] = p
}

// GetFloat64 returns the flag called "name".
// ok is false if the flag is not found or if it isn't a float.
func (f *Flags) GetFloat64(name string) (val float64, ok bool) {
	v, ok := f.flags[name]
	if !ok {
		return 0, false
	}
	ptr, ok := v.(*float64)
	if !ok {
		return 0, false
	}
	return *ptr, true
}

// Parse parses the given args into f.
// If the return value isn't empty, an error was encountered and the generated help is returned.
func (f *Flags) Parse(args []string) string {
	var b strings.Builder
	f.fs.SetOutput(&b)

	err := f.fs.Parse(args)
	if err != nil {
		return b.String()
	}
	return ""
}
