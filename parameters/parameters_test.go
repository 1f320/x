package parameters_test

import (
	"testing"

	"gitlab.com/1f320/x/parameters"
	"github.com/matryer/is"
)

func TestParameters(t *testing.T) {
	is := is.New(t)

	cases := []struct {
		Input               string
		FirstPop, SecondPop string
	}{
		{"Hello, world!", "Hello,", "world!"},
		{`"Hello world!"`, "Hello world!", ""},
	}

	for _, c := range cases {
		params := parameters.NewParameters(c.Input, true)

		is.Equal(c.FirstPop, params.Pop())
		is.Equal(c.SecondPop, params.Pop())
	}
}

func TestFlags(t *testing.T) {
	is := is.New(t)

	cases := []struct {
		Input string
		Flags []string
	}{
		{"----hello world! -show", []string{"hello", "show"}},
	}

	for _, c := range cases {
		params := parameters.NewParameters(c.Input, true)

		flags := params.Flags()

		for i := range c.Flags {
			is.Equal(c.Flags[i], flags[i])
		}
	}
}

func TestArgs(t *testing.T) {
	is := is.New(t)

	cases := []struct {
		Input      string
		MatchFlags bool
		Output     []string
	}{
		{"Hello, world!", true, []string{"Hello,", "world!"}},
		{`remindme "5h 3m" do a . thing™`, false, []string{"remindme", "5h 3m", "do", "a", ".", "thing™"}},
		{"members -r 204255221017214977", false, []string{"members", "-r", "204255221017214977"}},
		{"members -r 204255221017214977", true, []string{"members", "204255221017214977"}},
	}

	for _, c := range cases {
		params := parameters.NewParameters(c.Input, c.MatchFlags)
		is.Equal(c.Output, params.Args())
	}
}
