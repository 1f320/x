// Package strings provides a couple functions for strings, as an extension on the standard library's strings package.
package strings

import (
	"strings"
)

// TrimPrefixesSpace trims all given prefixes as well as whitespace from the given string
func TrimPrefixesSpace(s string, prefixes ...string) string {
	for _, prefix := range prefixes {
		s = strings.TrimPrefix(s, prefix)
		s = strings.TrimSpace(s)
	}
	return s
}

// HasAnyPrefix checks if the string has *any* of the given prefixes
func HasAnyPrefix(s string, prefixes ...string) bool {
	for _, prefix := range prefixes {
		if strings.HasPrefix(s, prefix) {
			return true
		}
	}
	return false
}

// HasAnySuffix checks if the string has *any* of the given suffixes
func HasAnySuffix(s string, suffixes ...string) bool {
	for _, suffix := range suffixes {
		if strings.HasSuffix(s, suffix) {
			return true
		}
	}
	return false
}

// EscapeBackticks escapes backticks in strings
func EscapeBackticks(s string) string {
	// Break all pairs of backticks by placing a ZWNBSP (U+FEFF) between them.
	// Run twice to catch any pairs that are created from the first pass
	s = strings.ReplaceAll(s, "``", "`\ufeff`")
	s = strings.ReplaceAll(s, "``", "`\ufeff`")

	// Escape the start/end of the string if necessary to better "connect" with other things
	if strings.HasPrefix(s, "`") {
		s = "\ufeff" + s
	}
	if strings.HasSuffix(s, "`") {
		s = s + "\ufeff"
	}

	return s
}

// AsCode returns the given string as code, correctly escaped
func AsCode(s string) string {
	return "``" + EscapeBackticks(s) + "``"
}
