package strings_test

import (
	"testing"

	"gitlab.com/1f320/x/strings"
	"github.com/matryer/is"
)

func TestTrimPrefixes(t *testing.T) {
	is := is.New(t)

	cases := []struct {
		in     string
		trim   []string
		expect string
	}{
		{"hello world!", []string{"hello"}, "world!"},
		// TrimSpace is called *after* trimming the prefix
		{"   this prefix will not be trimmed!", []string{"this prefix will"}, "this prefix will not be trimmed!"},
	}

	for _, c := range cases {
		out := strings.TrimPrefixesSpace(c.in, c.trim...)
		is.Equal(c.expect, out)
	}
}

func TestHasAnyPrefix(t *testing.T) {
	is := is.New(t)

	cases := []struct {
		in       string
		prefixes []string
		expect   bool
	}{
		{";remindme", []string{".", "x-", ";"}, true},
		{"hello", []string{"H", "ħ", "a"}, false},
	}

	for _, c := range cases {
		is.Equal(c.expect, strings.HasAnyPrefix(c.in, c.prefixes...))
	}
}

func TestHasAnySuffix(t *testing.T) {
	is := is.New(t)

	cases := []struct {
		in       string
		suffixes []string
		expect   bool
	}{
		{"hello!", []string{"!", "a"}, true},
		{"mmmmmm", []string{"n"}, false},
	}

	for _, c := range cases {
		is.Equal(c.expect, strings.HasAnySuffix(c.in, c.suffixes...))
	}
}
